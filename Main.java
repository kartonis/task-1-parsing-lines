package com.company;
import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        try {
            FileInputStream fstream = new FileInputStream(args[0]);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            Map<String, Integer> table = new TreeMap<String, Integer>();
            while ((strLine = br.readLine()) != null) {
                Split parseString = new Split();
                parseString.sellect(strLine, table);
            }
            Split printString = new Split();
            printString.printMap(table);
        } catch (IOException e) {
            System.out.println("Ошибка");
        }
        return;
    }
}