package com.company;
import java.util.*;
public class Split {
    //static Map<String, Integer> table = new TreeMap<String, Integer>();
    int freq = 0;
    String splitString = new String();

    public void sellect(String splitString, Map<String, Integer> table) {
        //Map<String, Integer> table = new TreeMap<String, Integer>();
        this.splitString = splitString;
        String[] words = this.splitString.split("\\s");
        for (int i = 0; i < words.length; ++i) {
            if (!table.containsKey(words[i])) {
                table.put(words[i], 1);
            } else {
                freq = table.get(words[i]);
                freq++;
                table.put(words[i], freq);
                freq = 0;
            }
        }
    }

    public Split() {
    }
    ;

    public void printMap(Map<String, Integer> table ) {
        Comparator<Integer> cmp = new Comparator<Integer>(){
            @Override
            public int compare(Integer a, Integer b)
            {
                return b.compareTo(a);
            }
        };
        SortedMap<Integer, Set<String>> forPrint = new TreeMap<Integer, Set<String>>(cmp);
        Set<String> keys = table.keySet();
        for (String i : keys) {
            Set<String> list = forPrint.get(table.get(i));
            if (list == null) {
                list = new HashSet<>();
                forPrint.put(table.get(i), list);
            }
            list.add(i);
        }
        for (Map.Entry<Integer, Set<String>> entry : forPrint.entrySet()) {
            for (String s : entry.getValue()){
                System.out.print(s + ";");
                double procent = (double)entry.getKey()/table.size()*100;
                System.out.print(entry.getKey() + ";");
                System.out.printf("%.2f\n",procent);
            }
        }
        }
    }